/* Output from p2c, the Pascal-to-C translator */
/* From input file "temp.p" */
#include <stdlib.h>
//#include <kernel.h>
#include <stdio.h>
#include <time.h>
#include <stdint.h>
#include <stdbool.h>
#include <string.h>
#include <math.h>

#include "p2c.h"


#define maxkeywords     27
#define alphalength     10
#define linelength      120
#define emax            322
#define emin            (-99)
#define kmax            15
#define tmax            100
#define bmax            20
#define amax            30
#define c2max           20
#define csmax           30
#define cmax            850
#define lmax            7
#define smax            600
#define ermax           58
#define omax            63

#define xmax            131071L
#define nmax            LONG_MAX

#define lineleng        136
#define linelimit       200
#define stacksize       1500


typedef enum {
  intcon, realcon, charcon, string, notsy, plus, minus, times, idiv, rdiv,
  imod, andsy, orsy, eql, neq, gtr, geq, lss, leq, lparent, rparent, lbrack,
  rbrack, comma, semicolon, period, colon, becomes, constsy, typesy, varsy,
  functionsy, proceduresy, arraysy, recordsy, programsy, ident, beginsy, ifsy,
  casesy, repeatsy, whilesy, forsy, endsy, elsesy, untilsy, ofsy, dosy, tosy,
  downtosy, thensy
} symbol;
/* p2c: temp.p, line 36:
 * Note: Line breaker spent 0.0 seconds, 5000 tries on line 39 [251] */
typedef long index_;

typedef Char alfa_[alphalength];
typedef enum {
  konstant, variable, type1, prozedure, funktion
} object;
typedef enum {
  notyp, ints, reals, bools, chars, arrays, records
} types;
typedef long symset[3];

typedef long typset;

typedef struct item {
  types typ;
  index_ iref;
} item;

typedef struct order {
  char f;
  char x;
  long y;
} order;


typedef struct _REC_tab {
  alfa_ name;
  index_ link;
  unsigned obj : 3;
/* p2c: temp.p, line 79:
 * Note: Field width for OBJ assumes enum object has 5 elements [105] */
  unsigned typ : 3;
/* p2c: temp.p, line 80:
 * Note: Field width for TYP assumes enum types has 7 elements [105] */
  Signed int iref : 18;
  unsigned normal : 1, lev : 3;
  long adr;
} _REC_tab;

typedef struct _REC_atab {
  unsigned inxtyp : 3, eltyp : 3;
/* p2c: temp.p, line 88:
 * Note: Field width for INXTYP assumes enum types has 7 elements [105] */
  Signed int eliref : 18, low : 18, high : 18, elsize : 18, size : 18;
} _REC_atab;

typedef struct _REC_btab {
  index_ last, lastpar, psize, vsize;
} _REC_btab;


Static symbol sy;
Static alfa_ id;
Static long inum;
Static double rnum;
Static long sleng;
Static Char ch;
Static Char line[linelength];
Static long cc, lc, ll;
Static long errs[ermax / 32 + 2];
Static long errpos;
Static alfa_ progname;
Static boolean iflag, oflag;
Static symset constbegsys, typebegsys, blockbegsys, facbegsys, statbegsys;
Static alfa_ key[maxkeywords];
Static symbol ksy[maxkeywords];
Static symbol sps[256];
Static long t, a, b, sx, c1, c2;
Static typset stantyps;
Static long display[lmax + 1];
Static _REC_tab tab[tmax + 1];
Static _REC_atab atab[amax];
Static _REC_btab btab[bmax];
Static Char stab[smax + 1];
Static double rconst[c2max];
Static order code[cmax + 1];


Static Void errormsg()
{
  long k;
  alfa_ msg[ermax + 1];
  long SET[257];

  /*errormsg*/
  memcpy(msg[0], "undef id  ", sizeof(alfa_));
  memcpy(msg[1], "multi def ", sizeof(alfa_));
  memcpy(msg[2], "identifier", sizeof(alfa_));
  memcpy(msg[3], "program   ", sizeof(alfa_));
  memcpy(msg[4], ")         ", sizeof(alfa_));
  memcpy(msg[5], ":         ", sizeof(alfa_));
  memcpy(msg[6], "syntax    ", sizeof(alfa_));
  memcpy(msg[7], "ident, var", sizeof(alfa_));
  memcpy(msg[8], "of        ", sizeof(alfa_));
  memcpy(msg[9], "(         ", sizeof(alfa_));
  memcpy(msg[10], "id, array ", sizeof(alfa_));
  memcpy(msg[11], "[         ", sizeof(alfa_));
  memcpy(msg[12], "]         ", sizeof(alfa_));
  memcpy(msg[13], "..        ", sizeof(alfa_));
  memcpy(msg[14], ";         ", sizeof(alfa_));
  memcpy(msg[15], "func. type", sizeof(alfa_));
  memcpy(msg[16], "=         ", sizeof(alfa_));
  memcpy(msg[17], "boolean   ", sizeof(alfa_));
  memcpy(msg[18], "convar typ", sizeof(alfa_));
  memcpy(msg[19], "type      ", sizeof(alfa_));
  memcpy(msg[20], "prog.param", sizeof(alfa_));
  memcpy(msg[21], "too big   ", sizeof(alfa_));
  memcpy(msg[22], ".         ", sizeof(alfa_));
  memcpy(msg[23], "typ (case)", sizeof(alfa_));
  memcpy(msg[24], "character ", sizeof(alfa_));
  memcpy(msg[25], "const id  ", sizeof(alfa_));
  memcpy(msg[26], "index type", sizeof(alfa_));
  memcpy(msg[27], "indexbound", sizeof(alfa_));
  memcpy(msg[28], "no array  ", sizeof(alfa_));
  memcpy(msg[29], "type id   ", sizeof(alfa_));
  memcpy(msg[30], "undef type", sizeof(alfa_));
  memcpy(msg[31], "no record ", sizeof(alfa_));
  memcpy(msg[32], "boole type", sizeof(alfa_));
  memcpy(msg[33], "arith type", sizeof(alfa_));
  memcpy(msg[34], "integer   ", sizeof(alfa_));
  memcpy(msg[35], "types     ", sizeof(alfa_));
  memcpy(msg[36], "param type", sizeof(alfa_));
  memcpy(msg[37], "variab typ", sizeof(alfa_));
  memcpy(msg[38], "string    ", sizeof(alfa_));
  memcpy(msg[39], "no.of pars", sizeof(alfa_));
  memcpy(msg[40], "type      ", sizeof(alfa_));
  memcpy(msg[41], "type      ", sizeof(alfa_));
  memcpy(msg[42], "real type ", sizeof(alfa_));
  memcpy(msg[43], "integer   ", sizeof(alfa_));
  memcpy(msg[44], "var, const", sizeof(alfa_));
  memcpy(msg[45], "var, proc ", sizeof(alfa_));
  memcpy(msg[46], "types (:=)", sizeof(alfa_));
  memcpy(msg[47], "typ (case)", sizeof(alfa_));
  memcpy(msg[48], "type      ", sizeof(alfa_));
  memcpy(msg[49], "store ovfl", sizeof(alfa_));
  memcpy(msg[50], "constant  ", sizeof(alfa_));
  memcpy(msg[51], ":=        ", sizeof(alfa_));
  memcpy(msg[52], "then      ", sizeof(alfa_));
  memcpy(msg[53], "until     ", sizeof(alfa_));
  memcpy(msg[54], "do        ", sizeof(alfa_));
  memcpy(msg[55], "to downto ", sizeof(alfa_));
  memcpy(msg[56], "begin     ", sizeof(alfa_));
  memcpy(msg[57], "end       ", sizeof(alfa_));
  memcpy(msg[58], "factor    ", sizeof(alfa_));
  k = 0;
  printf("\n key words\n");
  while (*errs != 0L) {
    while (!P_inset((int)k, errs))
      k++;
    printf("%12ld  %.*s\n", k, alphalength, msg[k]);
    P_remset(errs, (int)k);
  }  /*while*/
}


Static jmp_buf _JL99;


Static Void nextch()
{
  /* read the next character and the end of lines */
  /*nextch*/
  if (cc == ll) {   /*if*/
    if (P_eof(stdin)) {   /*if*/
      printf("\n program incomplete.\n");
      errormsg();
      longjmp(_JL99, 1);
    }
    if (errpos != 0) {   /*if*/
      putchar('\n');
      errpos = 0;
    }
    printf("%5ld  ", lc);
    ll = 0;
    cc = 0;
    while (!P_eoln(stdin)) {   /*while*/
      ll++;
      ch = getchar();
      if (ch == '\n')
	ch = ' ';
      putchar(ch);
      line[ll - 1] = ch;
    }
    putchar('\n');
    ll++;
    line[ll - 1] = getchar();
    if (line[ll - 1] == '\n')
      line[ll - 1] = ' ';
  }
  cc++;
  ch = line[cc - 1];
}


Static Void error(n)
long n;
{
  long SET[3];

  /*error*/
  if (errpos == 0)
    printf(" ****");
  if (cc <= errpos) {
    return;
  }  /*if*/
  printf("%*c@%2ld", (int)(cc - errpos), ' ', n);
  errpos = cc + 3;
  P_addset(errs, (int)n);
}


Static Void fatal(n)
long n;
{
  alfa_ msg[7];

  /*fatal*/
  putchar('\n');
  errormsg();
  memcpy(msg[0], "identifier", sizeof(alfa_));
  memcpy(msg[1], "procedures", sizeof(alfa_));
  memcpy(msg[2], "reals     ", sizeof(alfa_));
  memcpy(msg[3], "arrays    ", sizeof(alfa_));
  memcpy(msg[4], "levels    ", sizeof(alfa_));
  memcpy(msg[5], "code      ", sizeof(alfa_));
  memcpy(msg[6], "strings   ", sizeof(alfa_));
  printf(" compiler table for %.*s is too small\n", alphalength, msg[n - 1]);
  longjmp(_JL99, 1);   /* terminate compilation */
}


/* Local variables for insymbol: */
struct LOC_insymbol {
  long k, e;
} ;


Local Void readscale(LINK)
struct LOC_insymbol *LINK;
{
  long s, sign;

  /*readscale*/
  nextch();
  sign = 1;
  s = 0;
  if (ch == '+')
    nextch();
  else {
    if (ch == '-') {   /*if*/
      nextch();
      sign = -1;
    }
  }
  while (isdigit(ch)) {   /*while*/
    s = s * 10 + ch - '0';
    nextch();
  }
  LINK->e += s * sign;
}


Local Void adjustscale(LINK)
struct LOC_insymbol *LINK;
{
  long s;
  double d, t;

  /*adjustscale*/
  if (LINK->k + LINK->e > emax) {
    error(21L);
    return;
  }
  if (LINK->k + LINK->e < emin) {
    rnum = 0.0;
    return;
  }
  s = labs(LINK->e);
  t = 1.0;
  d = 10.0;
  do {
    while (!(s & 1)) {   /*while*/
      s /= 2;
      d *= d;
    }
    s--;
    t = d * t;
  } while (s != 0);
  if (LINK->e >= 0)
    rnum *= t;
  else
    rnum /= t;

  /*else*/
}


Static Void insymbol()
{
  /* reads next symbol */
  struct LOC_insymbol V;

  long i, j;


  /*insymbol*/
_L1:
  while (ch == ' ')
    nextch();
  if (islower(ch)) {  /*word*/
    V.k = 0;
    memcpy(id, "          ", sizeof(alfa_));
    do {
      if (V.k < alphalength) {   /*if*/
	V.k++;
	id[V.k - 1] = ch;
      }
      nextch();
    } while (islower(ch) || isdigit(ch));
    i = 1;
    j = maxkeywords;
    /* binary search */
    do {
      V.k = (i + j) / 2;
      if (strncmp(id, key[V.k - 1], sizeof(alfa_)) <= 0)
	j = V.k - 1;
      if (strncmp(id, key[V.k - 1], sizeof(alfa_)) >= 0)
	i = V.k + 1;
    } while (i <= j);
    if (i - 1 > j)
      sy = ksy[V.k - 1];
    else
      sy = ident;
    return;
  }  /*if*/
  if (isdigit(ch)) {  /* number */
    V.k = 0;
    inum = 0;
    sy = intcon;
    do {
      inum = inum * 10 + ch - '0';
      V.k++;
      nextch();
    } while (isdigit(ch));
    if (V.k > kmax || inum > nmax) {   /*if*/
      error(21L);
      inum = 0;
      V.k = 0;
    }
    if (ch == '.') {
      nextch();
      if (ch == '.') {
	ch = ':';
	return;
      }
      sy = realcon;
      rnum = inum;
      V.e = 0;
      while (isdigit(ch)) {   /*while*/
	V.e--;
	rnum = 10.0 * rnum + ch - '0';
	nextch();
      }
      if (ch == 'e')
	readscale(&V);
      if (V.e != 0)
	adjustscale(&V);
      return;
    }  /*if*/
    if (ch != 'e') {
      return;
    }  /*if*/
    sy = realcon;
    rnum = inum;
    V.e = 0;
    readscale(&V);
    if (V.e != 0)
      adjustscale(&V);
    return;
  }  /*if*/
  switch (ch) {

  case ':':   /*':'*/
    nextch();
    if (ch == '=') {
      sy = becomes;
      nextch();
    }  /*if*/
    else
      sy = colon;
    break;

  case '<':   /*'<'*/
    nextch();
    if (ch == '=') {
      sy = leq;
      nextch();
    }  /*if*/
    else {
      if (ch == '>') {
	sy = neq;
	nextch();
      }  /*if*/
      else
	sy = lss;
    }
    break;

  case '>':   /*'>'*/
    nextch();
    if (ch == '=') {
      sy = geq;
      nextch();
    }  /*if*/
    else
      sy = gtr;
    break;

  case '.':   /*'.'*/
    nextch();
    if (ch == '.') {
      sy = colon;
      nextch();
    }  /*if*/
    else
      sy = period;
    break;

  case '\'':   /*''''*/
    V.k = 0;
_L2:
    nextch();
    if (ch == '\'') {   /*if*/
      nextch();
      if (ch != '\'')
	goto _L3;
    }
    if (sx + V.k == smax)
      fatal(7L);
    stab[sx + V.k] = ch;
    V.k++;
    if (cc != 1) {  /* end of line */
      goto _L2;
    }  /*if*/
    V.k = 0;
_L3:
    if (V.k == 1) {
      sy = charcon;
      inum = stab[sx];
    }  /*if*/
    else {
      if (V.k == 0) {
	error(38L);
	sy = charcon;
	inum = 0;
      }  /*if*/
      else {
	sy = string;
	inum = sx;
	sleng = V.k;
	sx += V.k;
      }  /*else*/
    }
    break;

  case '(':   /*'('*/
    nextch();
    if (ch == '*') {
      do {
	while (ch != '*')
	  nextch();
	nextch();
      } while (ch != ')');
      nextch();
      goto _L1;
    }
    sy = lparent;
    break;

  case '+':
  case '-':
  case '*':
  case '/':
  case ')':
  case '=':
  case ',':
  case '[':
  case ']':
  case '#':
  case '&':
  case ';':   /*'+'*/
    sy = sps[ch];
    nextch();
    break;

  case '\\':
  case '%':
  case '@':
  case '$':
  case '!':
    error(24L);
    nextch();
    goto _L1;
    break;
    /*'\'*/
  }/*case*/

  /*else*/
  /* comment */
  /*else*/
}


Static Void enter(x0, x1, x2, x3)
Char *x0;
object x1;
types x2;
long x3;
{  /* enter standard identifier */
  _REC_tab *WITH;

  /*enter*/
  t++;
  WITH = &tab[t];
  memcpy(WITH->name, x0, sizeof(alfa_));
  WITH->link = t - 1;
  WITH->obj = (unsigned)x1;
  WITH->typ = (unsigned)x2;
  WITH->iref = 0;
  WITH->normal = true;
  WITH->lev = 0;
  WITH->adr = x3;   /*with*/
}


Static Void enterarray(tp, l, h)
types tp;
long l, h;
{
  _REC_atab *WITH;

  /*enterarray*/
  if (l > h)
    error(27L);
  if (labs(l) > xmax || labs(h) > xmax) {   /*if*/
    error(27L);
    l = 0;
    h = 0;
  }
  if (a == amax) {
    fatal(4L);
    return;
  }
  a++;
  WITH = &atab[a - 1];
  WITH->inxtyp = (unsigned)tp;
  WITH->low = l;
  WITH->high = h;   /*with*/

  /*else*/
}


Static Void enterblock()
{
  /*enterblock*/
  if (b == bmax) {
    fatal(2L);
    return;
  }
  b++;
  btab[b - 1].last = 0;
  btab[b - 1].lastpar = 0;

  /*else*/
}


Static Void enterreal(x)
double x;
{
  /*enterreal*/
  if (c2 == c2max - 1) {
    fatal(3L);
    return;
  }
  rconst[c2] = x;
  c1 = 1;
  while (rconst[c1 - 1] != x)
    c1++;
  if (c1 > c2)
    c2 = c1;

  /*else*/
}


Static Void emit(fct)
long fct;
{
  /*emit*/
  if (lc == cmax)
    fatal(6L);
  code[lc].f = fct;
  lc++;
}


Static Void emit1(fct, b)
long fct, b;
{
  order *WITH;

  /*emit1*/
  if (lc == cmax)
    fatal(6L);
  WITH = &code[lc];
  WITH->f = fct;
  WITH->y = b;   /*with*/
  lc++;
}


Static Void emit2(fct, a, b)
long fct, a, b;
{
  order *WITH;

  /*emit2*/
  if (lc == cmax)
    fatal(6L);
  WITH = &code[lc];
  WITH->f = fct;
  WITH->x = a;
  WITH->y = b;   /*with*/
  lc++;
}


Static Void printtables()
{
  long i;
  order o;
  long FORLIM;
  _REC_tab *WITH;
  _REC_btab *WITH1;
  _REC_atab *WITH2;

  /*printtables*/
  printf("0identifiers      link  obj  typ  iref  nrm  lev  adr\n");
  FORLIM = t;
  for (i = btab[0].last + 1; i <= FORLIM; i++) {
    WITH = &tab[i];
    printf("%12ld %.*s%5ld%5d%5d%5ld%5d%5d%5ld\n",
	   i, alphalength, WITH->name, WITH->link, (int)((object)WITH->obj),
	   (int)((types)WITH->typ), (long)SEXT(WITH->iref, 18), WITH->normal,
	   WITH->lev, WITH->adr);
  }
  printf("0blocks    last lpar psze vsze\n");
  FORLIM = b;
  for (i = 1; i <= FORLIM; i++) {
    WITH1 = &btab[i - 1];
    printf("%12ld%5ld%5ld%5ld%5ld\n",
	   i, WITH1->last, WITH1->lastpar, WITH1->psize, WITH1->vsize);
  }
  printf("0arrays    xtyp etyp eiref  low high elsz size\n");
  FORLIM = a;
  for (i = 1; i <= FORLIM; i++) {
    WITH2 = &atab[i - 1];
    printf("%12ld%5d%5d%5ld%5ld%5ld%5ld%5ld\n",
	   i, (int)((types)WITH2->inxtyp), (int)((types)WITH2->eltyp),
	   (long)SEXT(WITH2->eliref, 18), (long)SEXT(WITH2->low, 18),
	   (long)SEXT(WITH2->high, 18), (long)SEXT(WITH2->elsize, 18),
	   (long)SEXT(WITH2->size, 18));
  }
  printf("0code\n");
  FORLIM = lc;
  for (i = 0; i < FORLIM; i++) {   /*for*/
    if (i % 5 == 0)   /*if*/
      printf("\n%5ld", i);
/* p2c: temp.p, line 689:
 * Note: Using % for possibly-negative arguments [317] */
    o = code[i];
    printf("%5d", o.f);
    if (o.f < 31) {
      if (o.f < 4)
	printf("%2d%5ld", o.x, o.y);
      else
	printf("%7ld", o.y);
    } else
      printf("        ");
    putchar(',');
  }
  putchar('\n');
}


typedef struct conrec {
  types tp;
  union {
    long i;
    double r;
  } UU;
} conrec;


Static Void block PP((long *fsys, int isfun, long level));

typedef struct _REC_casetab {
  index_ val, lc;
} _REC_casetab;

/* Local variables for block: */
struct LOC_block {
  symset fsys;
  long level, dx;   /* data allocation index */
} ;

Local Void typ PP((long *fsys, types *tp, long *rf, long *sz,
		   struct LOC_block *LINK));
Local Void statement PP((long *fsys, struct LOC_block *LINK));


Local Void skip(fsys, n, LINK)
long *fsys;
long n;
struct LOC_block *LINK;
{
  /*skip*/
  error(n);
  while (!P_inset(sy, fsys))
    insymbol();
}


Local Void test(s1, s2, n, LINK)
long *s1, *s2;
long n;
struct LOC_block *LINK;
{
  symset SET;

  /*test*/
  if (!P_inset(sy, s1))
    skip(P_setunion(SET, s1, s2), n, LINK);
}


Local Void testsemicolon(LINK)
struct LOC_block *LINK;
{
  long SET[(long)ident / 32 + 2];
  symset SET1;

  /*testsemicolon*/
  if (sy == semicolon)
    insymbol();
  else {   /*else*/
    error(14L);
    if ((unsigned long)sy < 32 &&
	((1L << ((long)sy)) & ((1L << ((long)comma)) | (1L << ((long)colon)))) != 0)
      insymbol();
  }
  test(P_setunion(SET1, P_addset(P_expset(SET, 0L), (int)ident), blockbegsys),
       LINK->fsys, 6L, LINK);
}


Local Void enter_(id, k, LINK)
Char *id;
object k;
struct LOC_block *LINK;
{
  long j, l;
  _REC_tab *WITH;

  /*enter*/
  if (t == tmax) {
    fatal(1L);
    return;
  }
  memcpy(tab[0].name, id, sizeof(alfa_));
  j = btab[display[LINK->level] - 1].last;
  l = j;
  while (strncmp(tab[j].name, id, sizeof(alfa_)))
    j = tab[j].link;
  if (j != 0) {
    error(1L);
    return;
  }
  t++;
  WITH = &tab[t];
  memcpy(WITH->name, id, sizeof(alfa_));
  WITH->link = l;
  WITH->obj = (unsigned)k;
  WITH->typ = (unsigned)notyp;
  WITH->iref = 0;
  WITH->lev = LINK->level;
  WITH->adr = 0;   /*with*/
  btab[display[LINK->level] - 1].last = t;

  /*else*/
  /*else*/
}


Local long loc(id, LINK)
Char *id;
struct LOC_block *LINK;
{
  long i, j;

  /* locate id in table */

  /*loc*/
  i = LINK->level;
  memcpy(tab[0].name, id, sizeof(alfa_));
  do {
    j = btab[display[i] - 1].last;
    while (strncmp(tab[j].name, id, sizeof(alfa_)))
      j = tab[j].link;
    i--;
  } while (i >= 0 && j == 0);
  if (j == 0)
    error(0L);
  return j;
}


Local Void entervariable(LINK)
struct LOC_block *LINK;
{
  /*entervariable*/
  if (sy == ident) {
    enter_(id, variable, LINK);
    insymbol();
  }  /*if*/
  else
    error(2L);
}


Local Void constant(fsys, c, LINK)
long *fsys;
conrec *c;
struct LOC_block *LINK;
{
  long x, sign;
  symset SET;

  /*constant*/
  c->tp = notyp;
  c->UU.i = 0;
  test(constbegsys, fsys, 50L, LINK);
  if (!P_inset(sy, constbegsys)) {
    return;
  }  /*if*/
  if (sy == charcon) {
    c->tp = chars;
    c->UU.i = inum;
    insymbol();
  }  /*if*/
  else {   /*else*/
    sign = 1;
    if ((unsigned long)sy < 32 &&
	((1L << ((long)sy)) & ((1L << ((long)plus)) | (1L << ((long)minus)))) != 0)
    {   /*if*/
      if (sy == minus)
	sign = -1;
      insymbol();
    }
    if (sy == ident) {
      x = loc(id, LINK);
      if (x != 0) {
	if ((object)tab[x].obj != konstant)
	  error(25L);
	else {   /*else*/
	  c->tp = (types)tab[x].typ;
	  if (c->tp == reals)
	    c->UU.r = sign * rconst[tab[x].adr - 1];
	  else
	    c->UU.i = sign * tab[x].adr;
	}
      }
      insymbol();
    }  /*if*/
    else {
      if (sy == intcon) {
	c->tp = ints;
	c->UU.i = sign * inum;
	insymbol();
      }  /*if*/
      else {
	if (sy == realcon) {
	  c->tp = reals;
	  c->UU.r = sign * rnum;
	  insymbol();
	}  /*if*/
	else
	  skip(fsys, 50L, LINK);
      }
    }
  }
  test(fsys, P_expset(SET, 0L), 6L, LINK);
}

/* Local variables for typ: */
struct LOC_typ {
  struct LOC_block *LINK;
  symset fsys;
} ;


Local Void arraytyp(airef, arsz, LINK)
long *airef, *arsz;
struct LOC_typ *LINK;
{
  types eltp;
  conrec low, high;
  long elrf, elsz;
  long SET[(long)ofsy / 32 + 2];
  symset SET1;
  _REC_atab *WITH;

  /*arraytyp*/
  P_addset(P_expset(SET, 0L), (int)colon);
  P_addset(SET, (int)rbrack);
  P_addset(SET, (int)rparent);
  constant(P_setunion(SET1, P_addset(SET, (int)ofsy), LINK->fsys), &low,
	   LINK->LINK);
  if (low.tp == reals) {   /*if*/
    error(27L);
    low.tp = ints;
    low.UU.i = 0;
  }
  if (sy == colon)
    insymbol();
  else
    error(13L);
  P_addset(P_expset(SET, 0L), (int)rbrack);
  P_addset(SET, (int)comma);
  P_addset(SET, (int)rparent);
  constant(P_setunion(SET1, P_addset(SET, (int)ofsy), LINK->fsys), &high,
	   LINK->LINK);
  if (high.tp != low.tp) {   /*if*/
    error(27L);
    high.UU.i = low.UU.i;
  }
  enterarray(low.tp, low.UU.i, high.UU.i);
  *airef = a;
  if (sy == comma) {
    insymbol();
    eltp = arrays;
    arraytyp(&elrf, &elsz, LINK);
  }  /*if*/
  else {   /*else*/
    if (sy == rbrack)
      insymbol();
    else {   /*else*/
      error(12L);
      if (sy == rparent)
	insymbol();
    }
    if (sy == ofsy)
      insymbol();
    else
      error(8L);
    typ(LINK->fsys, &eltp, &elrf, &elsz, LINK->LINK);
  }
  WITH = &atab[*airef - 1];
  *arsz = (SEXT(WITH->high, 18) - SEXT(WITH->low, 18) + 1) * elsz;
  WITH->size = *arsz;
  WITH->eltyp = (unsigned)eltp;
  WITH->eliref = elrf;
  WITH->elsize = elsz;   /*with*/
}


Local Void typ(fsys_, tp, rf, sz, LINK)
long *fsys_;
types *tp;
long *rf, *sz;
struct LOC_block *LINK;
{
  struct LOC_typ V;
  long x;
  types eltp;
  long elrf, elsz, offset, t0, t1;
  _REC_tab *WITH;
  long SET[(long)endsy / 32 + 2];
  symset SET1;


  V.LINK = LINK;
  /*typ*/
  P_setcpy(V.fsys, fsys_);
  *tp = notyp;
  *rf = 0;
  *sz = 0;
  test(typebegsys, V.fsys, 10L, LINK);
  if (!P_inset(sy, typebegsys)) {
    return;
  }  /*if*/
  if (sy == ident) {
    x = loc(id, LINK);
    if (x != 0) {
      WITH = &tab[x];
      if ((object)WITH->obj != type1)
	error(29L);
      else {   /*else*/
	*tp = (types)WITH->typ;
	*rf = SEXT(WITH->iref, 18);
	*sz = WITH->adr;
	if (*tp == notyp)
	  error(30L);
      }
    }
    insymbol();
  }  /*if*/
  else {
    if (sy == arraysy) {
      insymbol();
      if (sy == lbrack)
	insymbol();
      else {   /*else*/
	error(11L);
	if (sy == lparent)
	  insymbol();
      }
      *tp = arrays;
      arraytyp(rf, sz, &V);
    }  /*if*/
    else   /*else*/
    {  /*records*/
      insymbol();
      enterblock();
      *tp = records;
      *rf = b;
      if (LINK->level == lmax)
	fatal(5L);
      LINK->level++;
      display[LINK->level] = b;
      offset = 0;
      while (sy != endsy)   /*while*/
      {  /* field secxtion */
	if (sy == ident) {   /*if*/
	  t0 = t;
	  entervariable(LINK);
	  while (sy == comma) {   /*while*/
	    insymbol();
	    entervariable(LINK);
	  }
	  if (sy == colon)
	    insymbol();
	  else
	    error(5L);
	  t1 = t;
	  P_addset(P_expset(SET, 0L), (int)semicolon);
	  P_addset(SET, (int)endsy);
	  P_addset(SET, (int)comma);
	  typ(P_setunion(SET1, V.fsys, P_addset(SET, (int)ident)), &eltp,
	      &elrf, &elsz, LINK);
	  while (t0 < t1) {
	    t0++;
	    WITH = &tab[t0];
	    WITH->typ = (unsigned)eltp;
	    WITH->iref = elrf;
	    WITH->normal = true;
	    WITH->adr = offset;
	    offset += elsz;   /*with*/
	  }  /*while*/
	}
	if (sy == endsy)   /*if*/
	  break;
	if (sy == semicolon)
	  insymbol();
	else {   /*else*/
	  error(14L);
	  if (sy == comma)
	    insymbol();
	}
	P_addset(P_expset(SET, 0L), (int)ident);
	P_addset(SET, (int)endsy);
	test(P_addset(SET, (int)semicolon), V.fsys, 6L, LINK);
      }
      btab[*rf - 1].vsize = offset;
      *sz = offset;
      btab[*rf - 1].psize = 0;
      insymbol();
      LINK->level--;
    }
  }
  test(V.fsys, P_expset(SET1, 0L), 6L, LINK);
}


Local Void parameterlist(LINK)
struct LOC_block *LINK;
{
  /* formal parameter list */
  types tp;
  long rf, sz, x, t0;
  boolean valpar;
  long SET[(long)ident / 32 + 2];
  symset SET1, SET2;
  _REC_tab *WITH;
  long SET3[(long)ident / 32 + 2];
  symset SET4;

  /*parameterlist*/
  insymbol();
  tp = notyp;
  rf = 0;
  sz = 0;
  P_addset(P_expset(SET, 0L), (int)ident);
  test(P_addset(SET, (int)varsy),
       P_setunion(SET2, LINK->fsys, P_expset(SET1, 1L << ((long)rparent))),
       7L, LINK);
  while (sy == (int)varsy || sy == (int)ident) {   /*while*/
    if (sy != varsy)
      valpar = true;
    else {   /*else*/
      insymbol();
      valpar = false;
    }
    t0 = t;
    entervariable(LINK);
    while (sy == comma) {   /*while*/
      insymbol();
      entervariable(LINK);
    }
    if (sy == colon) {
      insymbol();
      if (sy != ident)
	error(2L);
      else {   /*else*/
	x = loc(id, LINK);
	insymbol();
	if (x != 0) {
	  WITH = &tab[x];
	  if ((object)WITH->obj != type1)
	    error(29L);
	  else {   /*else*/
	    tp = (types)WITH->typ;
	    rf = SEXT(WITH->iref, 18);
	    if (valpar)
	      sz = WITH->adr;
	    else
	      sz = 1;
	  }
	}
      }
      P_addset(P_expset(SET, 0L), (int)comma);
      test(P_expset(SET1, (1L << ((long)semicolon)) | (1L << ((long)rparent))),
	   P_setunion(SET2, P_addset(SET, (int)ident), LINK->fsys), 14L,
	   LINK);
    }  /*if*/
    else
      error(5L);
    while (t0 < t) {   /*while*/
      t0++;
      WITH = &tab[t0];
      WITH->typ = (unsigned)tp;
      WITH->iref = rf;
      WITH->normal = valpar;
      WITH->adr = LINK->dx;
      WITH->lev = LINK->level;
      LINK->dx += sz;   /*with*/
    }
    if (sy == rparent) {
      break;
    }  /*if*/
    if (sy == semicolon)
      insymbol();
    else {   /*else*/
      error(14L);
      if (sy == comma)
	insymbol();
    }
    P_addset(P_expset(SET3, 0L), (int)ident);
    test(P_addset(SET3, (int)varsy),
	 P_setunion(SET4, P_expset(SET2, 1L << ((long)rparent)), LINK->fsys),
	 6L, LINK);
  }
  if (sy == rparent) {
    insymbol();
    test(P_expset(SET1, (1L << ((long)semicolon)) | (1L << ((long)colon))),
	 LINK->fsys, 6L, LINK);
  }  /*if*/
  else
    error(4L);
}


Local Void constantdeclaration(LINK)
struct LOC_block *LINK;
{
  conrec c;
  long SET[(long)ident / 32 + 2];
  symset SET1;

  /*constantdeclaration*/
  insymbol();
  test(P_addset(P_expset(SET, 0L), (int)ident), blockbegsys, 2L, LINK);
  while (sy == ident) {
    enter_(id, konstant, LINK);
    insymbol();
    if (sy == eql)
      insymbol();
    else {   /*else*/
      if (sy == becomes)
	insymbol();
    }
    P_addset(P_expset(SET, 0L), (int)semicolon);
    P_addset(SET, (int)comma);
    constant(P_setunion(SET1, P_addset(SET, (int)ident), LINK->fsys), &c,
	     LINK);
    tab[t].typ = (unsigned)c.tp;
    tab[t].iref = 0;
    if (c.tp == reals) {
      enterreal(c.UU.r);
      tab[t].adr = c1;
    }  /*if*/
    else
      tab[t].adr = c.UU.i;
    testsemicolon(LINK);
  }  /*while*/
}


Local Void typedeclaration(LINK)
struct LOC_block *LINK;
{
  types tp;
  long rf, sz, t1;
  long SET[(long)ident / 32 + 2];
  symset SET1;
  _REC_tab *WITH;

  /*typedeclaration*/
  insymbol();
  test(P_addset(P_expset(SET, 0L), (int)ident), blockbegsys, 2L, LINK);
  while (sy == ident) {
    enter_(id, type1, LINK);
    t1 = t;
    insymbol();
    if (sy == eql)
      insymbol();
    else {   /*else*/
      error(16L);
      if (sy == becomes)
	insymbol();
    }
    P_addset(P_expset(SET, 0L), (int)semicolon);
    P_addset(SET, (int)comma);
    typ(P_setunion(SET1, P_addset(SET, (int)ident), LINK->fsys), &tp, &rf,
	&sz, LINK);
    WITH = &tab[t1];
    WITH->typ = (unsigned)tp;
    WITH->iref = rf;
    WITH->adr = sz;   /*with*/
    testsemicolon(LINK);
  }  /*while*/
}


Local Void variabledeclaration(LINK)
struct LOC_block *LINK;
{
  long t0, t1, rf, sz;
  types tp;
  long SET[(long)ident / 32 + 2];
  symset SET1;
  _REC_tab *WITH;

  /*variabledeclaration*/
  insymbol();
  while (sy == ident) {
    t0 = t;
    entervariable(LINK);
    while (sy == comma) {   /*while*/
      insymbol();
      entervariable(LINK);
    }
    if (sy == colon)
      insymbol();
    else
      error(5L);
    t1 = t;
    P_addset(P_expset(SET, 0L), (int)semicolon);
    P_addset(SET, (int)comma);
    typ(P_setunion(SET1, P_addset(SET, (int)ident), LINK->fsys), &tp, &rf,
	&sz, LINK);
    while (t0 < t1) {   /*while*/
      t0++;
      WITH = &tab[t0];
      WITH->typ = (unsigned)tp;
      WITH->iref = rf;
      WITH->lev = LINK->level;
      WITH->adr = LINK->dx;
      WITH->normal = true;
      LINK->dx += sz;   /*with*/
    }
    testsemicolon(LINK);
  }  /*while*/
}


Local Void procdeclaration(LINK)
struct LOC_block *LINK;
{
  boolean isfun;
  symset SET, SET1;

  /*procdeclaration*/
  isfun = (sy == functionsy);
  insymbol();
  if (sy != ident) {   /*if*/
    error(2L);
    memcpy(id, "          ", sizeof(alfa_));
  }
  if (isfun)
    enter_(id, funktion, LINK);
  else
    enter_(id, prozedure, LINK);
  tab[t].normal = true;
  insymbol();
  block(P_setunion(SET1, P_expset(SET, 1L << ((long)semicolon)), LINK->fsys),
	isfun, LINK->level + 1);
  if (sy == semicolon)   /*exit*/
    insymbol();
  else
    error(14L);
  emit(isfun + 32L);
}

/* Local variables for statement: */
struct LOC_statement {
  struct LOC_block *LINK;
  symset fsys;
  long i;
  item x;
} ;

Local Void expression PP((long *fsys, item *x, struct LOC_statement *LINK));


Local Void selector(fsys, v, LINK)
long *fsys;
item *v;
struct LOC_statement *LINK;
{  /* sy in [lparent,lbrack,period] */
  long a, j;
  symset SET, SET1;

  /*selector*/
  do {
    if (sy == period) {  /* field selector */
      insymbol();
      if (sy != ident)
	error(2L);
      else {
	if (v->typ != records)
	  error(31L);
	else   /*else*/
	{  /* search field identifier */
	  j = btab[v->iref - 1].last;
	  memcpy(tab[0].name, id, sizeof(alfa_));
	  while (strncmp(tab[j].name, id, sizeof(alfa_)))
	    j = tab[j].link;
	  if (j == 0)
	    error(0L);
	  v->typ = (types)tab[j].typ;
	  v->iref = SEXT(tab[j].iref, 18);
	  a = tab[j].adr;
	  if (a != 0)
	    emit1(9L, a);
	}
	insymbol();
      }  /*else*/
    }  /*if*/
    else {  /* array selector */
      if (sy != lbrack)
	error(11L);
      do {
	insymbol();
	expression(P_setunion(SET1, fsys,
	    P_expset(SET, (1L << ((long)comma)) | (1L << ((long)rbrack)))),
	  &LINK->x, LINK);
	if (v->typ != arrays)
	  error(28L);
	else {
	  a = v->iref;
	  if ((types)atab[a - 1].inxtyp != LINK->x.typ)
	    error(26L);
	  else {
	    if (SEXT(atab[a - 1].elsize, 18) == 1)
	      emit1(20L, a);
	    else
	      emit1(21L, a);
	  }
	  v->typ = (types)atab[a - 1].eltyp;
	  v->iref = SEXT(atab[a - 1].eliref, 18);
	}  /*else*/
      } while (sy == comma);
      if (sy == rbrack)
	insymbol();
      else {
	error(12L);
	if (sy == rparent)
	  insymbol();
      }  /*else*/
    }  /*else*/
  } while ((unsigned long)sy < 32 &&
	   ((1L << ((long)sy)) & ((1L << ((long)lbrack)) |
	      (1L << ((long)lparent)) | (1L << ((long)period)))) != 0);
  test(fsys, P_expset(SET, 0L), 6L, LINK->LINK);
}


Local Void call(fsys, i, LINK)
long *fsys;
long i;
struct LOC_statement *LINK;
{   /* mark stack */
  item x;
  long lastp, cp, k;
  symset SET, SET1;

  /*call*/
  emit1(18L, i);
  lastp = btab[SEXT(tab[i].iref, 18) - 1].lastpar;
  cp = i;
  if (sy == lparent)   /*if*/
  {  /* actual parameter list */
    do {
      insymbol();
      if (cp >= lastp)
	error(39L);
      else {   /*else*/
	cp++;
	if (tab[cp].normal) {  /* value parameter */
	  expression(P_setunion(SET1, fsys, P_expset(SET,
			 (1L << ((long)comma)) | (1L << ((long)colon)) |
			 (1L << ((long)rparent)))), &x, LINK);
/* p2c: temp.p, line 2560: 
 * Note: Line breaker spent 1.0 seconds, 5000 tries on line 1487 [251] */
	  if (x.typ == (types)tab[cp].typ) {
	    if (x.iref != SEXT(tab[cp].iref, 18))
	      error(36L);
	    else {
	      if (x.typ == arrays)
		emit1(22L, (long)SEXT(atab[x.iref - 1].size, 18));
	      else {
		if (x.typ == records)
		  emit1(22L, btab[x.iref - 1].vsize);
	      }
	    }
	  }  /*if*/
	  else {
	    if (x.typ == ints && (types)tab[cp].typ == reals)
	      emit1(26L, 0L);
	    else {
	      if (x.typ != notyp)
		error(36L);
	    }
	  }
	}  /*if*/
	else {  /* varaiable parameter */
	  if (sy != ident)
	    error(2L);
	  else {
	    k = loc(id, LINK->LINK);
	    insymbol();
	    if (k != 0) {
	      if ((object)tab[k].obj != variable)
		error(37L);
	      x.typ = (types)tab[k].typ;
	      x.iref = SEXT(tab[k].iref, 18);
	      if (tab[k].normal)
		emit2(0L, (long)tab[k].lev, tab[k].adr);
	      else
		emit2(1L, (long)tab[k].lev, tab[k].adr);
	      if ((unsigned long)sy < 32 &&
		  ((1L << ((long)sy)) & ((1L << ((long)lbrack)) |
		     (1L << ((long)lparent)) | (1L << ((long)period)))) != 0)
		selector(P_setunion(SET1, fsys, P_expset(SET,
			     (1L << ((long)comma)) | (1L << ((long)colon)) |
			     (1L << ((long)rparent)))), &x, LINK);
/* p2c: temp.p, line 2560: 
 * Note: Line breaker spent 0.0 seconds, 5000 tries on line 1531 [251] */
	      if (x.typ != (types)tab[cp].typ ||
		  x.iref != SEXT(tab[cp].iref, 18))
		error(36L);
	    }  /*if*/
	  }  /*else*/
	}  /*else*/
      }
      test(P_expset(SET, (1L << ((long)comma)) | (1L << ((long)rparent))),
	   fsys, 6L, LINK->LINK);
    } while (sy == comma);
    if (sy == rparent)
      insymbol();
    else
      error(4L);
  }
  if (cp < lastp)
    error(39L);
  /* too few actual parameters */
  emit1(19L, btab[SEXT(tab[i].iref, 18) - 1].psize - 1);
  if (tab[i].lev < LINK->LINK->level)
    emit2(3L, (long)tab[i].lev, LINK->LINK->level);
}


Local types resulttype(a, b, LINK)
types a, b;
struct LOC_statement *LINK;
{
  types Result;

  /*resulttype*/
  if ((long)a > (long)reals || (long)b > (long)reals) {
    error(33L);
    return notyp;
  }  /*if*/
  if (a == notyp || b == notyp)
    return notyp;
  if (a == ints) {
    if (b == ints)
      return ints;
    Result = reals;
    emit1(26L, 1L);
    return Result;
  }
  Result = reals;
  if (b == ints)
    emit1(26L, 0L);
  return Result;

  /*else*/
  /*else*/
}

/* Local variables for expression: */
struct LOC_expression {
  struct LOC_statement *LINK;
} ;

/* Local variables for simpleexpression: */
struct LOC_simpleexpression {
  struct LOC_expression *LINK;
} ;

/* Local variables for term: */
struct LOC_term {
  struct LOC_simpleexpression *LINK;
} ;

/* Local variables for factor: */
struct LOC_factor {
  struct LOC_term *LINK;
  symset fsys;
  item *x;
  long i;
} ;


Local Void standfct(n, LINK)
long n;
struct LOC_factor *LINK;
{  /* standard function number n */
  typset ts;
  symset SET, SET1;

  /*standfct*/
  if (sy == lparent)
    insymbol();
  else
    error(9L);
  if (n < 17) {
    expression(P_setunion(SET1, LINK->fsys,
			  P_expset(SET, 1L << ((long)rparent))), LINK->x,
	       LINK->LINK->LINK->LINK->LINK);
    switch (n) {   /*case*/

    case 0:
    case 2:   /* abs */
      ts = (1L << ((long)ints)) | (1L << ((long)reals));
      tab[LINK->i].typ = (unsigned)LINK->x->typ;
      if (LINK->x->typ == reals)
	n++;
      break;
      /* sqr */
      /*0*/

    case 4:
    case 5:   /* odd */
      ts = 1L << ((long)ints);
      break;
      /* chr */

    case 6:   /* ord */
      ts = (1L << ((long)ints)) | (1L << ((long)bools)) | (1L << ((long)chars));
      break;

    case 7:
    case 8:   /* succ */
      ts = 1L << ((long)chars);
      break;
      /* pred */

    case 9:
    case 10:
    case 11:
    case 12:
    case 13:
    case 14:
    case 15:
    case 16:   /* round */
      /* co
      s   */
      ts = (1L << ((long)ints)) | (1L << ((long)reals));
      if (LINK->x->typ == ints)
	emit1(26L, 0L);
      break;
      /* trunc */
      /* sin   */
      /*9*/
    }
    if (((1L << ((long)LINK->x->typ)) & ts) != 0)
      emit1(8L, n);
    else {
      if (LINK->x->typ != notyp)
	error(48L);
    }
  }  /*if*/
  else   /*else*/
  {  /* n in [17,18] */
    if (sy != ident)
      error(2L);
    else {
      if (strncmp(id, "input     ", sizeof(alfa_)))
	error(0L);
      else
	insymbol();
    }
    emit1(8L, n);
  }
  LINK->x->typ = (types)tab[LINK->i].typ;
  if (sy == rparent)
    insymbol();
  else
    error(4L);
}


Local Void factor(fsys_, x_, LINK)
long *fsys_;
item *x_;
struct LOC_term *LINK;
{
  struct LOC_factor V;
  long f;
  _REC_tab *WITH;
  symset SET, SET1;


  V.LINK = LINK;
  /*factor*/
  P_setcpy(V.fsys, fsys_);
  V.x = x_;
  V.x->typ = notyp;
  V.x->iref = 0;
  test(facbegsys, V.fsys, 58L, LINK->LINK->LINK->LINK->LINK);
  while (P_inset(sy, facbegsys)) {
    if (sy == ident) {
      V.i = loc(id, LINK->LINK->LINK->LINK->LINK);
      insymbol();
      WITH = &tab[V.i];
      switch ((object)WITH->obj) {

      case konstant:   /*konstant*/
	V.x->typ = (types)WITH->typ;
	V.x->iref = 0;
	if (V.x->typ == reals) {
	  if (V.x->typ == reals)
	    emit1(25L, WITH->adr);
	  else
	    emit1(24L, WITH->adr);
	}
	break;

      case variable:   /*variable*/
	V.x->typ = (types)WITH->typ;
	V.x->iref = SEXT(WITH->iref, 18);
	if ((unsigned long)sy < 32 &&
	    ((1L << ((long)sy)) & ((1L << ((long)lbrack)) |
	       (1L << ((long)lparent)) | (1L << ((long)period)))) != 0) {
	  if (WITH->normal)
	    f = 0;
	  else
	    f = 1;
	  emit2(f, (long)WITH->lev, WITH->adr);
	  selector(V.fsys, V.x, LINK->LINK->LINK->LINK);
	  if (((1L << ((long)V.x->typ)) & stantyps) != 0)
	    emit(34L);
	}  /*if*/
	else {
	  if (((1L << ((long)V.x->typ)) & stantyps) != 0) {
	    if (WITH->normal)
	      f = 1;
	    else
	      f = 2;
	  } else {
	    if (WITH->normal)
	      f = 0;
	    else
	      f = 1;
	  }
	  emit2(f, (long)WITH->lev, WITH->adr);
	}  /*else*/
	break;

      case type1:
      case prozedure:
	error(44L);
	break;

      case funktion:
	V.x->typ = (types)WITH->typ;
	if (WITH->lev != 0)
	  call(V.fsys, V.i, LINK->LINK->LINK->LINK);
	else
	  standfct(WITH->adr, &V);
	break;
	/*funktion*/
      }/*case*/
    }  /*if*/
    else {
      if ((unsigned long)sy < 32 &&
	  ((1L << ((long)sy)) & ((1L << ((long)charcon)) |
	     (1L << ((long)intcon)) | (1L << ((long)realcon)))) != 0) {
	if (sy == realcon) {
	  V.x->typ = reals;
	  enterreal(rnum);
	  emit1(25L, c1);
	}  /*if*/
	else {   /*else*/
	  if (sy == charcon)
	    V.x->typ = chars;
	  else
	    V.x->typ = ints;
	  emit1(24L, inum);
	}
	V.x->iref = 0;
	insymbol();
      }  /*if*/
      else {
	if (sy == lparent) {
	  insymbol();
	  expression(P_setunion(SET1, V.fsys,
				P_expset(SET, 1L << ((long)rparent))), V.x,
		     LINK->LINK->LINK->LINK);
	  if (sy == rparent)
	    insymbol();
	  else
	    error(4L);
	}  /*if*/
	else {
	  if (sy == notsy) {   /*if*/
	    insymbol();
	    factor(V.fsys, V.x, LINK);
	    if (V.x->typ == bools)
	      emit(35L);
	    else {
	      if (V.x->typ != notyp)
		error(32L);
	    }
	  }
	}
      }
    }
    test(V.fsys, facbegsys, 6L, LINK->LINK->LINK->LINK->LINK);
  }  /*while*/
}


Local Void term(fsys, x, LINK)
long *fsys;
item *x;
struct LOC_simpleexpression *LINK;
{
  struct LOC_term V;
  item y;
  symbol op;
  symset SET, SET1;


  V.LINK = LINK;
  /*term*/
  factor(P_setunion(SET1, fsys, P_expset(SET,
	     (1L << ((long)times)) | (1L << ((long)rdiv)) | (1L << ((long)idiv)) |
	     (1L << ((long)imod)) | (1L << ((long)andsy)))), x, &V);
/* p2c: temp.p, line 2560: 
 * Note: Line breaker spent 1.0 seconds, 5000 tries on line 1846 [251] */
  while ((unsigned long)sy < 32 &&
	 ((1L << ((long)sy)) & ((1L << ((long)times)) | (1L << ((long)rdiv)) |
				(1L << ((long)idiv)) | (1L << ((long)imod)) |
				(1L << ((long)andsy)))) != 0) {
    op = sy;
    insymbol();
    factor(P_setunion(SET1, fsys,
	     P_expset(SET, (1L << ((long)times)) | (1L << ((long)rdiv)) |
			   (1L << ((long)idiv)) | (1L << ((long)imod)) |
			   (1L << ((long)andsy)))), &y, &V);
/* p2c: temp.p, line 2560: 
 * Note: Line breaker spent 1.0 seconds, 5000 tries on line 1858 [251] */
    if (op == times) {
      x->typ = resulttype(x->typ, y.typ, LINK->LINK->LINK);
      switch (x->typ) {

      case notyp:
	/* blank case */
	break;

      case ints:
	emit(57L);
	break;

      case reals:
	emit(60L);
	break;
      }/*case*/
      continue;
    }  /*if*/
    if (op == rdiv) {
      if (x->typ == ints) {   /*if*/
	emit1(26L, 1L);
	x->typ = reals;
      }
      if (y.typ == ints) {   /*if*/
	emit1(26L, 0L);
	y.typ = reals;
      }
      if (x->typ == reals && y.typ == reals)
	emit(61L);
      else {
	if (x->typ != notyp && y.typ != notyp)
	  error(33L);
	x->typ = notyp;
      }  /*else*/
      continue;
    }  /*if*/
    if (op == andsy) {
      if (x->typ == bools && y.typ == bools)
	emit(56L);
      else {
	if (x->typ != notyp && y.typ != notyp)
	  error(32L);
	x->typ = notyp;
      }  /*else*/
      continue;
    }  /*if*/
    if (x->typ == ints && y.typ == ints) {
      if (op == idiv)
	emit(58L);
      else
	emit(59L);
    } else {
      if (x->typ != notyp && y.typ != notyp)
	error(34L);
      x->typ = notyp;
    }  /*else*/
  }  /*while*/

  /* op in idiv,imod */
  /*else*/
}


Local Void simpleexpression(fsys, x, LINK)
long *fsys;
item *x;
struct LOC_expression *LINK;
{
  struct LOC_simpleexpression V;
  item y;
  symbol op;
  symset SET, SET1;


  V.LINK = LINK;
  /*simpleexpression*/
  if ((unsigned long)sy < 32 &&
      ((1L << ((long)sy)) & ((1L << ((long)plus)) | (1L << ((long)minus)))) != 0) {
    op = sy;
    insymbol();
    term(P_setunion(SET1, fsys, P_expset(SET,
		      (1L << ((long)plus)) | (1L << ((long)minus)))), x, &V);
    if ((long)x->typ > (long)reals)
      error(33L);
    else {
      if (op == minus)
	emit(36L);
    }
  }  /*if*/
  else
    term(P_setunion(SET1, fsys, P_expset(SET,
	     (1L << ((long)plus)) | (1L << ((long)minus)) | (1L << ((long)orsy)))),
	 x, &V);
  while ((unsigned long)sy < 32 &&
	 ((1L << ((long)sy)) & ((1L << ((long)plus)) | (1L << ((long)minus)) |
				(1L << ((long)orsy)))) != 0) {
    op = sy;
    insymbol();
    term(P_setunion(SET1, fsys, P_expset(SET,
	     (1L << ((long)plus)) | (1L << ((long)minus)) | (1L << ((long)orsy)))),
	 &y, &V);
    if (op == orsy) {
      if (x->typ == bools && y.typ == bools)
	emit(51L);
      else {
	if (x->typ != notyp && y.typ != notyp)
	  error(32L);
	x->typ = notyp;
      }  /*else*/
      continue;
    }  /*if*/
    x->typ = resulttype(x->typ, y.typ, LINK->LINK);
    switch (x->typ) {

    case notyp:
      /* blank case */
      break;

    case ints:
      if (op == plus)
	emit(52L);
      else
	emit(53L);
      break;

    case reals:
      if (op == plus)
	emit(54L);
      else
	emit(55L);
      break;
    }/*case*/
  }  /*while*/

  /*else*/
}


Local Void expression(fsys, x, LINK)
long *fsys;
item *x;
struct LOC_statement *LINK;
{
  struct LOC_expression V;
  item y;
  symbol op;
  symset SET, SET1;


  V.LINK = LINK;
  /*expression*/
  simpleexpression(P_setunion(SET1, fsys, P_expset(SET,
	(1L << ((long)eql)) | (1L << ((long)neq)) | (1L << ((long)lss)) |
	  (1L << ((long)leq)) | (1L << ((long)gtr)) | (1L << ((long)geq)))),
    x, &V);
/* p2c: temp.p, line 2560: 
 * Note: Line breaker spent 1.0 seconds, 5000 tries on line 2015 [251] */
  if ((unsigned long)sy >= 32 ||
      ((1L << ((long)sy)) & ((1L << ((long)eql)) | (1L << ((long)neq)) |
	 (1L << ((long)lss)) | (1L << ((long)leq)) | (1L << ((long)gtr)) |
	 (1L << ((long)geq)))) == 0) {
    return;
  }  /*if*/
  op = sy;
  insymbol();
  simpleexpression(fsys, &y, &V);
  if (((1L << ((long)x->typ)) & ((1L << ((long)notyp)) | (1L << ((long)ints)) |
	 (1L << ((long)bools)) | (1L << ((long)chars)))) != 0 &&
      x->typ == y.typ) {
    switch (op) {

    case eql:
      emit(45L);
      break;

    case neq:
      emit(46L);
      break;

    case lss:
      emit(47L);
      break;

    case leq:
      emit(48L);
      break;

    case gtr:
      emit(49L);
      break;

    case geq:
      emit(50L);
      break;
    }/*case*/
  } else {   /*else*/
    if (x->typ == ints) {
      x->typ = reals;
      emit1(26L, 1L);
    }  /*if*/
    else {
      if (y.typ == ints) {   /*if*/
	y.typ = reals;
	emit1(26L, 0L);
      }
    }
    if (x->typ == reals && y.typ == reals) {
      switch (op) {

      case eql:
	emit(39L);
	break;

      case neq:
	emit(40L);
	break;

      case lss:
	emit(41L);
	break;

      case leq:
	emit(42L);
	break;

      case gtr:
	emit(43L);
	break;

      case geq:
	emit(44L);
	break;
      }/*case*/
    } else
      error(35L);
  }
  x->typ = bools;


}


Local Void assignment(lv, ad, LINK)
long lv, ad;
struct LOC_statement *LINK;
{
  item x, y;
  long f;
  symset SET, SET1;

  /* tab[i].obj in [variable,prozedure]*/

  /*assignment*/
  x.typ = (types)tab[LINK->i].typ;
  x.iref = SEXT(tab[LINK->i].iref, 18);
  if (tab[LINK->i].normal)
    f = 0;
  else
    f = 1;
  emit2(f, lv, ad);
  if ((unsigned long)sy < 32 &&
      ((1L << ((long)sy)) & ((1L << ((long)lbrack)) |
	 (1L << ((long)lparent)) | (1L << ((long)period)))) != 0)
    selector(P_setunion(SET1,
	       P_expset(SET, (1L << ((long)becomes)) | (1L << ((long)eql))),
	       LINK->fsys), &x, LINK);
  if (sy == becomes)
    insymbol();
  else {   /*else*/
    error(51L);
    if (sy == eql)
      insymbol();
  }
  expression(LINK->fsys, &y, LINK);
  if (x.typ == y.typ) {
    if (((1L << ((long)x.typ)) & stantyps) != 0) {
      emit(38L);
      return;
    }
    if (x.iref != y.iref) {
      error(46L);
      return;
    }
    if (x.typ == arrays)
      emit1(23L, (long)SEXT(atab[x.iref - 1].size, 18));
    else
      emit1(23L, btab[x.iref - 1].vsize);
    return;
  }
  if (x.typ == reals && y.typ == ints) {
    emit1(26L, 0L);
    emit(38L);
  }  /*if*/
  else {
    if (x.typ != notyp && y.typ != notyp)
      error(46L);
  }
}


Local Void compoundstatement(LINK)
struct LOC_statement *LINK;
{
  symset SET, SET1;
  long SET2[(long)endsy / 32 + 2];
  symset SET3;

  /*compoundstatement*/
  insymbol();
  while (P_inset(sy, P_setunion(SET1, P_expset(SET, 1L << ((long)semicolon)),
				statbegsys)))
  {   /*while*/
    if (sy == semicolon)
      insymbol();
    else
      error(14L);
    P_addset(P_expset(SET2, 0L), (int)semicolon);
    statement(P_setunion(SET3, P_addset(SET2, (int)endsy), LINK->fsys),
	      LINK->LINK);
  }
  if (sy == endsy)
    insymbol();
  else
    error(57L);
}


Local Void ifstatement(LINK)
struct LOC_statement *LINK;
{
  item x;
  long lc1, lc2;
  symset SET, SET1;
  long SET2[(long)elsesy / 32 + 2];

  /*ifstatement*/
  insymbol();
  P_addset(P_expset(SET, 0L), (int)thensy);
  expression(P_setunion(SET1, LINK->fsys, P_addset(SET, (int)dosy)), &x, LINK);
  if (((1L << ((long)x.typ)) & ((1L << ((long)bools)) | (1L << ((long)notyp)))) == 0)
    error(17L);
  lc1 = lc;
  emit(11L);
  /* jmpc */
  if (sy == thensy)
    insymbol();
  else {   /*else*/
    error(52L);
    if (sy == dosy)
      insymbol();
  }
  statement(P_setunion(SET, LINK->fsys,
		       P_addset(P_expset(SET2, 0L), (int)elsesy)),
	    LINK->LINK);
  if (sy != elsesy) {
    code[lc1].y = lc;
    return;
  }  /*if*/
  insymbol();
  lc2 = lc;
  emit(10L);
  code[lc1].y = lc;
  statement(LINK->fsys, LINK->LINK);
  code[lc2].y = lc;
}

/* Local variables for casestatement: */
struct LOC_casestatement {
  struct LOC_statement *LINK;
  item x;
  long i, j;
  _REC_casetab casetab[csmax];
  long exittab[csmax];
} ;


Local Void caselabel(LINK)
struct LOC_casestatement *LINK;
{
  conrec lab;
  long k;
  symset SET, SET1;

  /*caselabel*/
  constant(P_setunion(SET1, LINK->LINK->fsys,
	     P_expset(SET, (1L << ((long)comma)) | (1L << ((long)colon)))),
	   &lab, LINK->LINK->LINK);
  if (lab.tp != LINK->x.typ) {
    error(47L);
    return;
  }
  if (LINK->i == csmax) {
    fatal(6L);
    return;
  }
  LINK->i++;
  k = 0;
  LINK->casetab[LINK->i - 1].val = lab.UU.i;
  LINK->casetab[LINK->i - 1].lc = lc;
  do {
    k++;
  } while (LINK->casetab[k - 1].val != lab.UU.i);
  if (k < LINK->i)
    error(1L);
  /* multiple def */

  /*else*/
}


Local Void onecase(LINK)
struct LOC_casestatement *LINK;
{
  long SET[(long)endsy / 32 + 2];
  symset SET1;

  /*onecase*/
  if (!P_inset(sy, constbegsys)) {
    return;
  }  /*if*/
  caselabel(LINK);
  while (sy == comma) {   /*while*/
    insymbol();
    caselabel(LINK);
  }
  if (sy == colon)
    insymbol();
  else
    error(5L);
  P_addset(P_expset(SET, 0L), (int)semicolon);
  statement(P_setunion(SET1, P_addset(SET, (int)endsy), LINK->LINK->fsys),
	    LINK->LINK->LINK);
  LINK->j++;
  LINK->exittab[LINK->j - 1] = lc;
  emit(10L);
}


Local Void casestatement(LINK)
struct LOC_statement *LINK;
{
  struct LOC_casestatement V;
  long k, lc1;
  long SET[(long)ofsy / 32 + 2];
  symset SET1;
  long FORLIM;


  V.LINK = LINK;
  /*casestatement*/
  insymbol();
  V.i = 0;
  V.j = 0;
  P_addset(P_expset(SET, 0L), (int)ofsy);
  P_addset(SET, (int)comma);
  expression(P_setunion(SET1, LINK->fsys, P_addset(SET, (int)colon)), &V.x,
	     LINK);
  if (((1L << ((long)V.x.typ)) & ((1L << ((long)ints)) | (1L << ((long)bools)) |
	 (1L << ((long)chars)) | (1L << ((long)notyp)))) == 0)
    error(23L);
  lc1 = lc;
  emit(12L);
  /* jmpx */
  if (sy == ofsy)
    insymbol();
  else
    error(8L);
  onecase(&V);
  while (sy == semicolon) {   /*while*/
    insymbol();
    onecase(&V);
  }
  code[lc1].y = lc;
  FORLIM = V.i;
  for (k = 0; k < FORLIM; k++) {   /*for*/
    emit1(13L, V.casetab[k].val);
    emit1(13L, V.casetab[k].lc);
  }
  emit1(10L, 0L);
  FORLIM = V.j;
  for (k = 0; k < FORLIM; k++)
    code[V.exittab[k]].y = lc;
  if (sy == endsy)
    insymbol();
  else
    error(57L);
}


Local Void repeatstatement(LINK)
struct LOC_statement *LINK;
{
  item x;
  long lc1;
  long SET[(long)untilsy / 32 + 2];
  symset SET1, SET2, SET3;

  /*repeatstatement*/
  lc1 = lc;
  insymbol();
  P_addset(P_expset(SET, 0L), (int)semicolon);
  statement(P_setunion(SET1, P_addset(SET, (int)untilsy), LINK->fsys),
	    LINK->LINK);
  while (P_inset(sy, P_setunion(SET2, P_expset(SET1, 1L << ((long)semicolon)),
				statbegsys)))
  {   /*while*/
    if (sy == semicolon)
      insymbol();
    else
      error(14L);
    P_addset(P_expset(SET, 0L), (int)semicolon);
    statement(P_setunion(SET3, P_addset(SET, (int)untilsy), LINK->fsys),
	      LINK->LINK);
  }
  if (sy != untilsy) {
    error(53L);
    return;
  }  /*if*/
  insymbol();
  expression(LINK->fsys, &x, LINK);
  if (((1L << ((long)x.typ)) & ((1L << ((long)bools)) | (1L << ((long)notyp)))) == 0)
    error(17L);
  emit1(11L, lc1);
}


Local Void whilestatement(LINK)
struct LOC_statement *LINK;
{
  item x;
  long lc1, lc2;
  long SET[(long)dosy / 32 + 2];
  symset SET1;

  /*whilestatement*/
  insymbol();
  lc1 = lc;
  expression(P_setunion(SET1, LINK->fsys,
			P_addset(P_expset(SET, 0L), (int)dosy)), &x, LINK);
  if (((1L << ((long)x.typ)) & ((1L << ((long)bools)) | (1L << ((long)notyp)))) == 0)
    error(17L);
  lc2 = lc;
  emit(11L);
  if (sy == dosy)
    insymbol();
  else
    error(54L);
  statement(LINK->fsys, LINK->LINK);
  emit1(10L, lc1);
  code[lc1].y = lc;
}


Local Void forstatement(LINK)
struct LOC_statement *LINK;
{
  types cvt;
  item x;
  long i, f, lc1, lc2;
  long SET[(long)downtosy / 32 + 2];
  symset SET1;
  long SET2[(long)dosy / 32 + 2];

  /*forstatement*/
  insymbol();
  if (sy == ident) {
    i = loc(id, LINK->LINK);
    insymbol();
    if (i == 0)
      cvt = ints;
    else {
      if ((object)tab[i].obj == variable) {
	cvt = (types)tab[i].typ;
	emit2(0L, (long)tab[i].lev, tab[i].adr);
	if (((1L << ((long)cvt)) & ((1L << ((long)notyp)) | (1L << ((long)ints)) |
	       (1L << ((long)bools)) | (1L << ((long)chars)))) == 0)
	  error(18L);
      }  /*if*/
      else {
	error(37L);
	cvt = ints;
      }  /*else*/
    }
  }  /*if*/
  else {
    P_addset(P_expset(SET, 0L), (int)becomes);
    P_addset(SET, (int)tosy);
    P_addset(SET, (int)downtosy);
    skip(P_setunion(SET1, P_addset(SET, (int)dosy), LINK->fsys), 2L,
	 LINK->LINK);
  }
  if (sy == becomes) {
    insymbol();
    P_addset(P_expset(SET, 0L), (int)tosy);
    P_addset(SET, (int)downtosy);
    expression(P_setunion(SET1, P_addset(SET, (int)dosy), LINK->fsys), &x,
	       LINK);
    if (x.typ != cvt)
      error(19L);
  }  /*if*/
  else {
    P_addset(P_expset(SET, 0L), (int)tosy);
    P_addset(SET, (int)downtosy);
    skip(P_setunion(SET1, P_addset(SET, (int)dosy), LINK->fsys), 51L,
	 LINK->LINK);
  }
  f = 14;
  if (sy == (int)downtosy || sy == (int)tosy) {
    if (sy == downtosy)
      f = 16;
    insymbol();
    expression(P_setunion(SET1, P_addset(P_expset(SET2, 0L), (int)dosy),
			  LINK->fsys), &x, LINK);
    if (x.typ != cvt)
      error(19L);
  }  /*if*/
  else
    skip(P_setunion(SET1, P_addset(P_expset(SET2, 0L), (int)dosy), LINK->fsys),
	 55L, LINK->LINK);
  lc1 = lc;
  emit(f);
  if (sy == dosy)
    insymbol();
  else
    error(54L);
  lc2 = lc;
  statement(LINK->fsys, LINK->LINK);
  emit1(f + 1, lc2);
  code[lc1].y = lc;
}


Local Void standproc(n, LINK)
long n;
struct LOC_statement *LINK;
{
  long i, f;
  item x, y;
  symset SET, SET1;

  /*standproc*/
  switch (n) {

  case 1:
  case 2:   /*1*/
    /* read */
    if (!iflag) {   /*if*/
      error(20L);
      iflag = true;
    }
    if (sy == lparent) {   /*if*/
      do {
	insymbol();
	if (sy != ident)
	  error(2L);
	else {   /*else*/
	  i = loc(id, LINK->LINK);
	  insymbol();
	  if (i != 0) {
	    if ((object)tab[i].obj != variable)
	      error(37L);
	    else {
	      x.typ = (types)tab[i].typ;
	      x.iref = SEXT(tab[i].iref, 18);
	      if (tab[i].normal)
		f = 0;
	      else
		f = 1;
	      emit2(f, (long)tab[i].lev, tab[i].adr);
	      if ((unsigned long)sy < 32 &&
		  ((1L << ((long)sy)) & ((1L << ((long)lbrack)) |
		     (1L << ((long)lparent)) | (1L << ((long)period)))) != 0)
		selector(P_setunion(SET1, LINK->fsys, P_expset(SET,
		      (1L << ((long)comma)) | (1L << ((long)rparent)))), &x,
		  LINK);
	      if (((1L << ((long)x.typ)) &
		   ((1L << ((long)ints)) | (1L << ((long)reals)) |
		    (1L << ((long)chars)) | (1L << ((long)notyp)))) != 0)
		emit1(27L, (long)x.typ);
	      else
		error(40L);
	    }  /*else*/
	  }
	}
	test(P_expset(SET, (1L << ((long)comma)) | (1L << ((long)rparent))),
	     LINK->fsys, 6L, LINK->LINK);
      } while (sy == comma);
      if (sy == rparent)
	insymbol();
      else
	error(4L);
    }
    if (n == 2)
      emit(62L);
    break;

  case 3:
  case 4:  /*write*/
    if (sy == lparent) {   /*if*/
      do {
	insymbol();
	if (sy == string) {
	  emit1(24L, sleng);
	  emit1(28L, inum);
	  insymbol();
	}  /*if*/
	else {
	  expression(P_setunion(SET1, LINK->fsys, P_expset(SET,
			 (1L << ((long)comma)) | (1L << ((long)colon)) |
			 (1L << ((long)rparent)))), &x, LINK);
/* p2c: temp.p, line 2560: 
 * Note: Line breaker spent 0.0 seconds, 5000 tries on line 2570 [251] */
	  if (((1L << ((long)x.typ)) & stantyps) == 0)
	    error(41L);
	  if (sy == colon) {
	    insymbol();
	    expression(P_setunion(SET1, LINK->fsys, P_expset(SET,
			   (1L << ((long)comma)) | (1L << ((long)colon)) |
			   (1L << ((long)rparent)))), &y, LINK);
/* p2c: temp.p, line 2560: 
 * Note: Line breaker spent 1.0 seconds, 5000 tries on line 2579 [251] */
	    if (y.typ != ints)
	      error(43L);
	    if (sy == colon) {
	      if (x.typ != reals)
		error(42L);
	      insymbol();
	      expression(P_setunion(SET1, LINK->fsys,
		  P_expset(SET,
			   (1L << ((long)comma)) | (1L << ((long)rparent)))),
		&y, LINK);
	      if (y.typ != ints)
		error(43L);
	      emit(37L);
	    }  /*if*/
	    else
	      emit1(30L, (long)x.typ);
	  }  /*if*/
	  else
	    emit1(29L, (long)x.typ);
	}  /*else*/
      } while (sy == comma);
      if (sy == rparent)
	insymbol();
      else
	error(4L);
    }
    if (n == 4)
      emit(63L);
    break;
    /*3*/
  }/*case*/
}




Local Void statement(fsys_, LINK)
long *fsys_;
struct LOC_block *LINK;
{
  struct LOC_statement V;
  long SET[(long)ident / 32 + 2];
  symset SET1;


  V.LINK = LINK;
  /*statement*/
  P_setcpy(V.fsys, fsys_);
  if (P_inset(sy, P_setunion(SET1, statbegsys,
			     P_addset(P_expset(SET, 0L), (int)ident)))) {
    switch (sy) {   /*case*/

    case ident:   /*ident*/
      V.i = loc(id, LINK);
      insymbol();
      if (V.i != 0) {
	switch ((object)tab[V.i].obj) {

	case konstant:
	case type1:
	  error(45L);
	  break;

	case variable:
	  assignment((long)tab[V.i].lev, tab[V.i].adr, &V);
	  break;

	case prozedure:
	  if (tab[V.i].lev != 0)
	    call(V.fsys, V.i, &V);
	  else
	    standproc(tab[V.i].adr, &V);
	  break;

	case funktion:
	  if (SEXT(tab[V.i].iref, 18) == display[LINK->level])
	    assignment(tab[V.i].lev + 1L, 0L, &V);
	  else
	    error(45L);
	  break;
	}/*case*/
      }
      break;

    case beginsy:
      compoundstatement(&V);
      break;

    case ifsy:
      ifstatement(&V);
      break;

    case casesy:
      casestatement(&V);
      break;

    case whilesy:
      whilestatement(&V);
      break;

    case repeatsy:
      repeatstatement(&V);
      break;

    case forsy:
      forstatement(&V);
      break;
    }
  }
  test(V.fsys, P_expset(SET1, 0L), 14L, LINK);
}


Static Void block(fsys_, isfun, level_)
long *fsys_;
boolean isfun;
long level_;
{
  struct LOC_block V;
  long prt;   /* t-index of this procedure */
  long prb;   /* b-index of this procedure */
  long x;
  symset SET, SET1;
  long SET2[(long)beginsy / 32 + 2];
  long SET3[(long)endsy / 32 + 2];
  symset SET4;


  /*block*/
  P_setcpy(V.fsys, fsys_);
  V.level = level_;
  V.dx = 5;
  prt = t;
  if (V.level > lmax)
    fatal(5L);
  test(P_expset(SET, (1L << ((long)lparent)) | (1L << ((long)colon)) |
		     (1L << ((long)semicolon))), V.fsys, 7L, &V);
  enterblock();
  display[V.level] = b;
  prb = b;
  tab[prt].typ = (unsigned)notyp;
  tab[prt].iref = prb;
  if (sy == lparent)
    parameterlist(&V);
  btab[prb - 1].lastpar = t;
  btab[prb - 1].psize = V.dx;
  if (isfun) {
    if (sy == colon) {   /* function type */
      insymbol();
      if (sy == ident) {
	x = loc(id, &V);
	insymbol();
	if (x != 0) {
	  if ((object)tab[x].obj != type1)
	    error(29L);
	  else {
	    if (((1L << tab[x].typ) & stantyps) != 0)
	      tab[prt].typ = (unsigned)((types)tab[x].typ);
	    else
	      error(15L);
	  }
	}
      }  /*if*/
      else
	skip(P_setunion(SET1, P_expset(SET, 1L << ((long)semicolon)), V.fsys),
	     2L, &V);
    }  /*if*/
    else
      error(5L);
  }
  if (sy == semicolon)
    insymbol();
  else
    error(14L);
  do {
    if (sy == constsy)
      constantdeclaration(&V);
    if (sy == typesy)
      typedeclaration(&V);
    if (sy == varsy)
      variabledeclaration(&V);
    btab[prb - 1].vsize = V.dx;
    while (sy == (int)functionsy || sy == (int)proceduresy)
      procdeclaration(&V);
    test(P_addset(P_expset(SET2, 0L), (int)beginsy),
	 P_setunion(SET, blockbegsys, statbegsys), 56L, &V);
  } while (!P_inset(sy, statbegsys));
  tab[prt].adr = lc;
  insymbol();
  P_addset(P_expset(SET3, 0L), (int)semicolon);
  statement(P_setunion(SET, P_addset(SET3, (int)endsy), V.fsys), &V);
  while (P_inset(sy, P_setunion(SET1, P_expset(SET, 1L << ((long)semicolon)),
				statbegsys)))
  {   /*while*/
    if (sy == semicolon)
      insymbol();
    else
      error(14L);
    P_addset(P_expset(SET3, 0L), (int)semicolon);
    statement(P_setunion(SET4, P_addset(SET3, (int)endsy), V.fsys), &V);
  }
  if (sy == endsy)
    insymbol();
  else
    error(57L);
  test(P_setunion(SET1, V.fsys, P_expset(SET, 1L << ((long)period))),
       P_expset(SET4, 0L), 6L, &V);
}


typedef union _REC_s {
  long i;
  double r;
  boolean b;
  Char c;
} _REC_s;


Static Void interpret()
{
  order ir;
  long pc;
  enum {
    run, fin, caschk, divchk, inxchk, stkchk, linchk, lngchk, redchk
  } ps;
  long t, b, lncnt, ocnt, blkcnt, chrcnt, h1, h2, h3, h4;
  long fld[4];
  long display[lmax];
  _REC_s s[stacksize];
  long TEMP;
  double TEMP1;
  _REC_tab *WITH;

  /*interpret*/
  s[0].i = 0;
  s[1].i = 0;
  s[2].i = -1;
  s[3].i = btab[0].last;
  b = 0;
  display[0] = 0;
  t = btab[1].vsize - 1;
  pc = tab[s[3].i].adr;
  ps = run;
  lncnt = 0;
  ocnt = 0;
  chrcnt = 0;
  fld[0] = 10;
  fld[1] = 22;
  fld[2] = 10;
  fld[3] = 1;
  do {
    ir = code[pc];
    pc++;
    ocnt++;
    switch (ir.f) {   /* case */

    case 0:   /*0*/
      /* load address */
      t++;
      if (t > stacksize)
	ps = stkchk;
      else
	s[t - 1].i = display[ir.x - 1] + ir.y;
      break;

    case 1:   /*1*/
      /* load value */
      t++;
      if (t > stacksize)
	ps = stkchk;
      else
	s[t - 1] = s[display[ir.x - 1] + ir.y - 1];
      break;

    case 2:   /*2*/
      /* load indirect */
      t++;
      if (t > stacksize)
	ps = stkchk;
      else
	s[t - 1] = s[s[display[ir.x - 1] + ir.y - 1].i - 1];
      break;

    case 3:   /*3*/
      /* update display */
      h1 = ir.y;
      h2 = ir.x;
      h3 = b;
      do {
	display[h1 - 1] = h3;
	h1--;
	h3 = s[h3 + 1].i;
      } while (h1 != h2);
      break;

    case 8:
      switch (ir.y) {   /*case*/

      case 0:
	s[t - 1].i = labs(s[t - 1].i);
	break;

      case 1:
	s[t - 1].r = fabs(s[t - 1].r);
	break;

      case 2:
	TEMP = s[t - 1].i;
	s[t - 1].i = TEMP * TEMP;
	break;

      case 3:
	TEMP1 = s[t - 1].r;
	s[t - 1].r = TEMP1 * TEMP1;
	break;

      case 4:
	s[t - 1].b = s[t - 1].i & 1;
	break;

      case 5:   /*5*/
	/* s[t].c := chr(s[t].i); */
	if ((unsigned long)s[t - 1].i > 63)
	  ps = inxchk;
	break;

      case 6:   /* s[t].i:=ord(s[t].c) */
	break;

      case 7:
	s[t - 1].c++;
	break;

      case 8:
	s[t - 1].c--;
	break;

      case 9:
	s[t - 1].i = (long)floor(s[t - 1].r + 0.5);
	break;

      case 10:
	s[t - 1].i = (long)s[t - 1].r;
	break;

      case 11:
	s[t - 1].r = sin(s[t - 1].r);
	break;

      case 12:
	s[t - 1].r = cos(s[t - 1].r);
	break;

      case 13:
	s[t - 1].r = exp(s[t - 1].r);
	break;

      case 14:
	s[t - 1].r = log(s[t - 1].r);
	break;

      case 15:
	s[t - 1].r = sqrt(s[t - 1].r);
	break;

      case 16:
	s[t - 1].r = atan(s[t - 1].r);
	break;

      case 17:   /*17*/
	t++;
	if (t > stacksize)
	  ps = stkchk;
	else
	  s[t - 1].b = P_eof(stdin);
	break;

      case 18:   /*18*/
	t++;
	if (t > stacksize)
	  ps = stkchk;
	else
	  s[t - 1].b = P_eoln(stdin);
	break;
      }
      break;

    case 9:
      s[t - 1].i += ir.y;   /* offset */
      break;

    case 10:
      pc = ir.y;   /* jump */
      break;

    case 11:   /*11*/
      /* conditional jump */
      if (!s[t - 1].b)
	pc = ir.y;
      t--;
      break;

    case 12:   /*12*/
      /* switch */
      h1 = s[t - 1].i;
      t--;
      h2 = ir.y;
      h3 = 0;
      do {
	if (code[h2].f != 13) {
	  h3 = 1;
	  ps = caschk;
	}  /*if*/
	else {
	  if (code[h2].y == h1) {
	    h3 = 1;
	    pc = code[h2 + 1].y;
	  }  /*if*/
	  else
	    h2 += 2;
	}
      } while (h3 == 0);
      break;

    case 14:   /*14*/
      /* for1up*/
      h1 = s[t - 2].i;
      if (h1 <= s[t - 1].i)
	s[s[t - 3].i - 1].i = h1;
      else {
	t -= 3;
	pc = ir.y;
      }  /*else*/
      break;

    case 15:   /*15*/
      /* for2up */
      h2 = s[t - 3].i;
      h1 = s[h2 - 1].i + 1;
      if (h1 <= s[t - 1].i) {
	s[h2 - 1].i = h1;
	pc = ir.y;
      }  /*if*/
      else
	t -= 3;
      break;

    case 16:   /*16*/
      /*for1down*/
      h1 = s[t - 2].i;
      if (h1 >= s[t - 1].i)
	s[s[t - 3].i - 1].i = h1;
      else {
	pc = ir.y;
	t -= 3;
      }  /*else*/
      break;

    case 17:   /*17*/
      /*for2down*/
      h2 = s[t - 3].i;
      h1 = s[h2 - 1].i - 1;
      if (h1 >= s[t - 1].i) {
	s[h2 - 1].i = h1;
	pc = ir.y;
      }  /*if*/
      else
	t -= 3;
      break;

    case 18:   /*18*/
      /* marck stack*/
      h1 = btab[SEXT(tab[ir.y].iref, 18) - 1].vsize;
      if (t + h1 > stacksize)
	ps = stkchk;
      else {
	t += 5;
	s[t - 2].i = h1 - 1;
	s[t - 1].i = ir.y;
      }  /*else*/
      break;

    case 19:   /*19*/
      /* call */
      h1 = t - ir.y;   /*h1 points to base */
      h2 = s[h1 + 3].i;   /*h2 points to tab */
      h3 = tab[h2].lev;
      display[h3] = h1;
      h4 = s[h1 + 2].i + h1;
      s[h1].i = pc;
      s[h1 + 1].i = display[h3 - 1];
      s[h1 + 2].i = b;
      for (h3 = t; h3 < h4; h3++)
	s[h3].i = 0;
      b = h1;
      t = h4;
      pc = tab[h2].adr;
      break;

    case 20:   /*20*/
      /* index1 */
      h1 = ir.y;   /* h1 points to atab */
      h2 = SEXT(atab[h1 - 1].low, 18);
      h3 = s[t - 1].i;
      if (h3 < h2)
	ps = inxchk;
      else {
	if (h3 > SEXT(atab[h1 - 1].high, 18))
	  ps = inxchk;
	else {
	  t--;
	  s[t - 1].i += h3 - h2;
	}  /*else*/
      }
      break;

    case 21:   /*21*/
      /* index */
      h1 = ir.y;   /* h1 points to atab */
      h2 = SEXT(atab[h1 - 1].low, 18);
      h3 = s[t - 1].i;
      if (h3 < h2)
	ps = inxchk;
      else {
	if (h3 > SEXT(atab[h1 - 1].high, 18))
	  ps = inxchk;
	else {
	  t--;
	  s[t - 1].i += (h3 - h2) * SEXT(atab[h1 - 1].elsize, 18);
	}  /*else*/
      }
      break;

    case 22:   /*22*/
      /* load block */
      h1 = s[t - 1].i;
      t--;
      h2 = ir.y + t;
      if (h2 > stacksize)
	ps = stkchk;
      else {
	while (t < h2) {
	  t++;
	  s[t - 1] = s[h1 - 1];
	  h1++;
	}  /*while*/
      }
      break;

    case 23:   /*23*/
      /* copy block */
      h1 = s[t - 2].i;
      h2 = s[t - 1].i;
      h3 = h1 + ir.y;
      while (h1 < h3) {   /*while*/
	s[h1 - 1] = s[h2 - 1];
	h1++;
	h2++;
      }
      t -= 2;
      break;

    case 24:   /*24*/
      /* literal */
      t--;
      if (t > stacksize)
	ps = stkchk;
      else
	s[t - 1].i = ir.y;
      break;

    case 25:   /*25*/
      /* load real */
      t--;
      if (t > stacksize)
	ps = stkchk;
      else
	s[t - 1].r = rconst[ir.y - 1];
      break;

    case 26:   /*26*/
      /* float */
      h1 = t - ir.y;
      s[h1 - 1].r = s[h1 - 1].i;
      break;

    case 27:   /*27*/
      /* read */
      if (P_eof(stdin))
	ps = redchk;
      else {
	switch (ir.y) {   /*case*/

	case 1:
	  scanf("%ld", &s[s[t - 1].i - 1].i);
	  break;

	case 2:
	  scanf("%lg", &s[s[t - 1].i - 1].r);
	  break;

	case 3:
	  s[s[t - 1].i - 1].c = getchar();
	  if (s[s[t - 1].i - 1].c == '\n')
	    s[s[t - 1].i - 1].c = ' ';
	  break;
	}
      }
      t--;
      break;

    case 28:   /*28*/
      /* write string */
      h1 = s[t - 1].i;
      h2 = ir.y;
      t--;
      chrcnt += h1;
      if (chrcnt > lineleng)
	ps = lngchk;
      do {
	putchar(stab[h2]);
	h1--;
	h2++;
      } while (h1 != 0);
      break;

    case 29:   /*29*/
      /* write1 */
      chrcnt += fld[ir.y - 1];
      if (chrcnt > lineleng)
	ps = lngchk;
      else {
	switch (ir.y) {   /*case*/

	case 1:
	  printf("%*ld", (int)fld[0], s[t - 1].i);
	  break;

	case 2:
	  printf("% .*E", P_max((int)fld[1] - 7, 1), s[t - 1].r);
	  break;

	case 3:
	  printf("%*s", (int)fld[2], s[t - 1].b ? "TRUE" : "FALSE");
	  break;

	case 4:
	  putchar(s[t - 1].c);
	  break;
	}
      }
      t--;
      break;

    case 30:   /*30*/
      /* write2 */
      chrcnt += s[t - 1].i;
      if (chrcnt > lineleng)
	ps = lngchk;
      else {
	switch (ir.y) {   /*case*/

	case 1:
	  printf("%*ld", (int)s[t - 1].i, s[t - 2].i);
	  break;

	case 2:
	  printf("% .*E", P_max((int)s[t - 1].i - 7, 1), s[t - 2].r);
	  break;

	case 3:
	  printf("%*s", (int)s[t - 1].i, s[t - 2].b ? "TRUE" : "FALSE");
	  break;

	case 4:
	  printf("%*c", (int)s[t - 1].i, s[t - 2].c);
	  break;
	}
      }
      t -= 2;
      break;

    case 31:
      ps = fin;
      break;

    case 32:   /*32*/
      /* exit procedure */
      t = b - 1;
      pc = s[b].i;
      b = s[b + 2].i;
      break;

    case 33:   /*33*/
      /* exit function */
      t = b;
      pc = s[b].i;
      b = s[b + 2].i;
      break;

    case 34:
      s[t - 1] = s[s[t - 1].i - 1];
      break;

    case 35:
      s[t - 1].b = !s[t - 1].b;
      break;

    case 36:
      s[t - 1].i = -s[t - 1].i;
      break;

    case 37:   /*37*/
      chrcnt += s[t - 2].i;
      if (chrcnt > lineleng)
	ps = lngchk;
      else
	printf("%*.*f", (int)s[t - 2].i, (int)s[t - 1].i, s[t - 3].r);
      t -= 3;
      break;

    case 38:   /*38*/
      /* store */
      s[s[t - 2].i - 1] = s[t - 1];
      t -= 2;
      break;

    case 39:   /*39*/
      t--;
      s[t - 1].b = (s[t - 1].r == s[t].r);
      break;

    case 40:   /*40*/
      t--;
      s[t - 1].b = (s[t - 1].r != s[t].r);
      break;

    case 41:   /*41*/
      t--;
      s[t - 1].b = (s[t - 1].r < s[t].r);
      break;

    case 42:   /*42*/
      t--;
      s[t - 1].b = (s[t - 1].r <= s[t].r);
      break;

    case 43:   /*43*/
      t--;
      s[t - 1].b = (s[t - 1].r > s[t].r);
      break;

    case 44:   /*44*/
      t--;
      s[t - 1].b = (s[t - 1].r >= s[t].r);
      break;

    case 45:   /*45*/
      t--;
      s[t - 1].b = (s[t - 1].i == s[t].i);
      break;

    case 46:   /*46*/
      t--;
      s[t - 1].b = (s[t - 1].i != s[t].i);
      break;

    case 47:   /*47*/
      t--;
      s[t - 1].b = (s[t - 1].i < s[t].i);
      break;

    case 48:   /*48*/
      t--;
      s[t - 1].b = (s[t - 1].i <= s[t].i);
      break;

    case 49:   /*49*/
      t--;
      s[t - 1].b = (s[t - 1].i > s[t].i);
      break;

    case 50:   /*50*/
      t--;
      s[t - 1].b = (s[t - 1].i >= s[t].i);
      break;

    case 51:   /*51*/
      t--;
      s[t - 1].b = (s[t - 1].b || s[t].b);
      break;

    case 52:   /*52*/
      t--;
      s[t - 1].i += s[t].i;
      break;

    case 53:   /*53*/
      t--;
      s[t - 1].i -= s[t].i;
      break;

    case 54:   /*54*/
      t--;
      s[t - 1].r += s[t].r;
      break;

    case 55:   /*55*/
      t--;
      s[t - 1].r -= s[t].r;
      break;

    case 56:   /*56*/
      t--;
      s[t - 1].b = (s[t - 1].b && s[t].b);
      break;

    case 57:   /*57*/
      t--;
      s[t - 1].i *= s[t].i;
      break;

    case 58:   /*58*/
      t--;
      if (s[t].i == 0)
	ps = divchk;
      else
	s[t - 1].i /= s[t].i;
      break;

    case 59:   /*59*/
      t--;
      if (s[t].i == 0)
	ps = divchk;
      else {
	s[t - 1].i %= s[t].i;
/* p2c: temp.p, line 3116:
 * Note: Using % for possibly-negative arguments [317] */
      }
      break;

    case 60:   /*60*/
      t--;
      s[t - 1].r *= s[t].r;
      break;

    case 61:   /*61*/
      t--;
      s[t - 1].r /= s[t].r;
      break;

    case 62:
      if (P_eof(stdin))
	ps = redchk;
      else {
	scanf("%*[^\n]");
	getchar();
      }
      break;

    case 63:
      putchar('\n');
      lncnt++;
      chrcnt = 0;
      if (lncnt > linelimit)
	ps = linchk;
      break;
      /*63*/
    }
  } while (ps == run);
  if (ps != fin) {   /*if*/
    printf("\n0halt at %5ld because of ", pc);
    switch (ps) {   /*case*/

    case caschk:
      printf("undefined case\n");
      break;

    case divchk:
      printf("division by 0\n");
      break;

    case inxchk:
      printf("storage overflow\n");
      break;

    case linchk:
      printf("too much output\n");
      break;

    case lngchk:
      printf("line too long\n");
      break;

    case redchk:
      printf("reading past end of file\n");
      break;
    }
    h1 = b;
    blkcnt = 10;   /* post mortem dump */
    do {
      putchar('\n');
      blkcnt--;
      if (blkcnt == 0)
	h1 = 0;
      h2 = s[h1 + 3].i;
      if (h1 != 0)
	printf(" %.*s  called at%5ld\n", alphalength, tab[h2].name, s[h1].i);
      h2 = btab[SEXT(tab[h2].iref, 18) - 1].last;
      while (h2 != 0) {
	WITH = &tab[h2];
	if ((object)WITH->obj == variable) {
	  if (((1L << WITH->typ) & stantyps) != 0) {   /*if*/
	    printf("    %.*s = ", alphalength, WITH->name);
	    if (WITH->normal)
	      h3 = h1 + WITH->adr;
	    else
	      h3 = s[h1 + WITH->adr - 1].i;
	    switch ((types)WITH->typ) {

	    case ints:
	      printf("%12ld\n", s[h3 - 1].i);
	      break;

	    case reals:
	      printf("% .5E\n", s[h3 - 1].r);
	      break;

	    case bools:
	      puts(s[h3 - 1].b ? " TRUE" : "FALSE");
	      break;

	    case chars:
	      printf("%c\n", s[h3 - 1].c);
	      break;
	    }/*case*/
	  }
	}
	h2 = WITH->link;   /*with*/
      }
      h1 = s[h1 + 2].i;
    } while (h1 >= 0);
  }
  printf("\n%12ld steps\n", ocnt);
}


int main(int argc, char *argv[])
//int argc;
//Char *argv[];
{  /* main program */
  symset SET;
  _REC_btab *WITH;

  PASCAL_MAIN(argc, argv);
  /*pascals*/
  if (setjmp(_JL99))
    goto _L99;
  putchar('\n');
  memcpy(key[0], "and       ", sizeof(alfa_));
  memcpy(key[1], "array     ", sizeof(alfa_));
  memcpy(key[2], "begin     ", sizeof(alfa_));
  memcpy(key[3], "case      ", sizeof(alfa_));
  memcpy(key[4], "const     ", sizeof(alfa_));
  memcpy(key[5], "div       ", sizeof(alfa_));
  memcpy(key[6], "downto    ", sizeof(alfa_));
  memcpy(key[7], "do        ", sizeof(alfa_));
  memcpy(key[8], "else      ", sizeof(alfa_));
  memcpy(key[9], "end       ", sizeof(alfa_));
  memcpy(key[10], "for       ", sizeof(alfa_));
  memcpy(key[11], "function  ", sizeof(alfa_));
  memcpy(key[12], "if        ", sizeof(alfa_));
  memcpy(key[13], "mod       ", sizeof(alfa_));
  memcpy(key[14], "not       ", sizeof(alfa_));
  memcpy(key[15], "of        ", sizeof(alfa_));
  memcpy(key[16], "or        ", sizeof(alfa_));
  memcpy(key[17], "procedure ", sizeof(alfa_));
  memcpy(key[18], "program   ", sizeof(alfa_));
  memcpy(key[19], "record    ", sizeof(alfa_));
  memcpy(key[20], "repeat    ", sizeof(alfa_));
  memcpy(key[21], "then      ", sizeof(alfa_));
  memcpy(key[22], "to        ", sizeof(alfa_));
  memcpy(key[23], "type      ", sizeof(alfa_));
  memcpy(key[24], "until     ", sizeof(alfa_));
  memcpy(key[25], "var       ", sizeof(alfa_));
  memcpy(key[26], "while     ", sizeof(alfa_));
  ksy[0] = andsy;
  ksy[1] = arraysy;
  ksy[2] = beginsy;
  ksy[3] = casesy;
  ksy[4] = constsy;
  ksy[5] = idiv;
  ksy[6] = downtosy;
  ksy[7] = dosy;
  ksy[8] = elsesy;
  ksy[9] = endsy;
  ksy[10] = forsy;
  ksy[11] = functionsy;
  ksy[12] = ifsy;
  ksy[13] = imod;
  ksy[14] = notsy;
  ksy[15] = ofsy;
  ksy[16] = orsy;
  ksy[17] = proceduresy;
  ksy[18] = programsy;
  ksy[19] = recordsy;
  ksy[20] = repeatsy;
  ksy[21] = thensy;
  ksy[22] = tosy;
  ksy[23] = typesy;
  ksy[24] = untilsy;
  ksy[25] = varsy;
  ksy[26] = whilesy;
  sps['+'] = plus;
  sps['-'] = minus;
  sps['*'] = times;
  sps['/'] = rdiv;
  sps['='] = eql;
  sps['['] = lbrack;
  sps[']'] = rbrack;
  sps['&'] = andsy;
  sps['('] = lparent;
  sps[')'] = rparent;
  sps[','] = comma;
  sps['#'] = neq;
  sps[';'] = semicolon;
  P_addset(P_expset(constbegsys, 0L), (int)plus);
  P_addset(constbegsys, (int)minus);
  P_addset(constbegsys, (int)intcon);
  P_addset(constbegsys, (int)realcon);
  P_addset(constbegsys, (int)charcon);
  P_addset(constbegsys, (int)ident);
  P_addset(P_expset(typebegsys, 0L), (int)ident);
  P_addset(typebegsys, (int)arraysy);
  P_addset(typebegsys, (int)recordsy);
  P_addset(P_expset(blockbegsys, 0L), (int)constsy);
  P_addset(blockbegsys, (int)typesy);
  P_addset(blockbegsys, (int)varsy);
  P_addset(blockbegsys, (int)proceduresy);
  P_addset(blockbegsys, (int)functionsy);
  P_addset(blockbegsys, (int)beginsy);
  P_addset(P_expset(facbegsys, 0L), (int)intcon);
  P_addset(facbegsys, (int)realcon);
  P_addset(facbegsys, (int)charcon);
  P_addset(facbegsys, (int)ident);
  P_addset(facbegsys, (int)lparent);
  P_addset(facbegsys, (int)notsy);
  P_addset(P_expset(statbegsys, 0L), (int)beginsy);
  P_addset(statbegsys, (int)ifsy);
  P_addset(statbegsys, (int)whilesy);
  P_addset(statbegsys, (int)repeatsy);
  P_addset(statbegsys, (int)forsy);
  P_addset(statbegsys, (int)casesy);
  stantyps = (1L << ((long)notyp)) | (1L << ((long)ints)) |
      (1L << ((long)reals)) | (1L << ((long)bools)) | (1L << ((long)chars));
  lc = 0;
  ll = 0;
  cc = 0;
  ch = ' ';
  errpos = 0;
  P_expset(errs, 0L);
  insymbol();
  t = -1;
  a = 0;
  b = 1;
  sx = 0;
  c2 = 0;
  display[0] = 1;
  iflag = false;
  oflag = false;
  if (sy != programsy)
    error(3L);
  else {   /*else*/
    insymbol();
    if (sy != ident)
      error(2L);
    else {
      memcpy(progname, id, sizeof(alfa_));
      insymbol();
      if (sy != lparent)
	error(9L);
      else {
	do {
	  insymbol();
	  if (sy != ident)
	    error(2L);
	  else {
	    if (!strncmp(id, "input     ", sizeof(alfa_)))
	      iflag = true;
	    else {
	      if (!strncmp(id, "output    ", sizeof(alfa_)))
		oflag = true;
	      else
		error(0L);
	    }
	    insymbol();
	  }  /*else*/
	} while (sy == comma);
      }
      if (sy == rparent)
	insymbol();
      else
	error(4L);
      if (!oflag)
	error(20L);
    }  /*else*/
  }
  enter("          ", variable, notyp, 0L);
  enter("false     ", konstant, bools, 0L);
  enter("true      ", konstant, bools, 1L);
  enter("real      ", type1, reals, 1L);
  enter("char      ", type1, chars, 1L);
  enter("boolean   ", type1, bools, 1L);
  enter("integer   ", type1, ints, 1L);
  enter("abs       ", funktion, reals, 0L);
  enter("sqr       ", funktion, reals, 2L);
  enter("odd       ", funktion, bools, 4L);
  enter("chr       ", funktion, chars, 5L);
  enter("ord       ", funktion, ints, 6L);
  enter("succ      ", funktion, chars, 7L);
  enter("pred      ", funktion, chars, 8L);
  enter("round     ", funktion, ints, 9L);
  enter("trunc     ", funktion, ints, 10L);
  enter("sin       ", funktion, reals, 11L);
  enter("cos       ", funktion, reals, 12L);
  enter("exp       ", funktion, reals, 13L);
  enter("ln        ", funktion, reals, 14L);
  enter("sqrt      ", funktion, reals, 15L);
  enter("arctan    ", funktion, reals, 16L);
  enter("eof       ", funktion, bools, 17L);
  enter("eoln      ", funktion, bools, 18L);
  enter("read      ", prozedure, notyp, 1L);
  enter("readln    ", prozedure, notyp, 2L);
  enter("write     ", prozedure, notyp, 3L);
  enter("writeln   ", prozedure, notyp, 4L);
  enter("          ", prozedure, notyp, 0L);
  WITH = btab;
  WITH->last = t;
  WITH->lastpar = 1;
  WITH->psize = 0;
  WITH->vsize = 0;   /*with*/
  block(P_setunion(SET, blockbegsys, statbegsys), false, 1L);
  if (sy != period)   /* halt */
    error(22L);
  emit(31L);
  if (btab[1].vsize > stacksize)
    error(49L);
  if (!strncmp(progname, "test0     ", sizeof(alfa_)))
    printtables();
  if (*errs == 0L) {
    if (iflag) {   /*if*/
      if (P_eof(stdin))
	printf(" input data missing\n");
      else {
	printf(" (eor) \n");   /* copy input data */
	while (!P_eof(stdin)) {   /*while*/
	  putchar(' ');
	  while (!P_eoln(stdin)) {   /*while*/
	    ch = getchar();
	    if (ch == '\n')
	      ch = ' ';
	    putchar(ch);
	  }
	  putchar('\n');
	  ch = getchar();
	  if (ch == '\n')
	    ch = ' ';
	}
      }  /*else*/
    }
    printf(" (eof) \n");
    interpret();
  }  /*if*/
  else
    errormsg();
_L99:
  exit(EXIT_SUCCESS);
}



/* End. */
